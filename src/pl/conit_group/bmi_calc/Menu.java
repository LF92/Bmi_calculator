package pl.conit_group.bmi_calc;

/**
 * Created by lukasz on 22.05.17.
 */
class Menu {

    private static final String START_TEXT = "W celu wyliczenia wskaźnika masy ciała (BMI) potrzebne będą " +
            "następujące dane: waga [kg] oraz wzrost [cm]\n";
    private static final String MENU_TEXT = "\nDostępne akcje:\n1 - Oblicz BMI\n0 - Wyjdź\nwybierz: ";
    private static final String ENTER_WEIGHT = "Wpisz swoją wagę [kg] i zatwierdź klawiszem ENTER: ";
    private static final String ENTER_HEIGHT = "Wpisz swój wzrost [cm] i zatwierdź klawiszem ENTER: ";
    private static final String WRONG_DATA = "Wprowadzono nieprawidłowe dane, spróbuj ponownie";


    static void showStartText() {
        System.out.println(START_TEXT);
    }

    static void showMenuText() {
        System.out.println(MENU_TEXT);
    }

    static void showEnterWeightText() {
        System.out.print(ENTER_WEIGHT);
    }

    static void showEnterHeightText() {
        System.out.print(ENTER_HEIGHT);
    }

    static void showWrongData() {
        System.out.println(WRONG_DATA);
    }

    static void showBmiInfo(double bmi) {
        System.out.printf("Twoje bmi wynosi: %.2f, oznacza to %s\n", bmi, BMI_Info(bmi));
    }

    private static String BMI_Info(double bmi) {
        if (bmi < 16.00)
            return "wygłoszenie";
        else if (bmi >= 16.00 && bmi < 17.00)
            return "wychudzenie";
        else if (bmi >= 17.00 && bmi < 18.50)
            return "niedowagę";
        else if (bmi >= 18.50 && bmi < 25.00)
            return "wartość prawidłową";
        else if (bmi >= 25.00 && bmi < 30.00)
            return "nadwagę";
        else if (bmi >= 30.00 && bmi < 35.00)
            return "I stopień otyłości";
        else if (bmi >= 35.00 && bmi < 40.00)
            return "II stopień otyłości (otyłość kliniczna)";
        else if (bmi >= 40.00)
            return "III stopień otyłości (otyłość skrajna)";
        else
            return "Błąd";
    }
}