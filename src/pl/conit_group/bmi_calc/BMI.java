package pl.conit_group.bmi_calc;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by lukasz on 22.05.17.
 */
public class BMI {

    private static Scanner scanner = new Scanner(System.in);
    private short height;
    private short weight;

    void calculateBMI() {
        boolean userChoice = true;

        Menu.showStartText();

        do {
            Menu.showMenuText();

            switch (getMenuChoice()) {
                case 1:
                    getValueFromUser();
                    break;
                case 0:
                    userChoice = false;
                    scanner.close();
                    break;
                default:
                    System.out.println("brak akcji");
                    break;
            }
        } while (userChoice);
    }

    private void getValueFromUser() {
        Menu.showEnterHeightText();
        height = getVal();

        Menu.showEnterWeightText();
        weight = getVal();

        if (getMBI() != 0.0)
            Menu.showBmiInfo(getMBI());
        else {
            Menu.showWrongData();
            getValueFromUser();
        }
    }

    private double cmToMeter(int cm) {
        return (double) cm / 100;
    }

    private double getMBI() {
        if (cmToMeter(height) != 0)
            return weight / (cmToMeter(height) * cmToMeter(height));
        else
            return 0.0;
    }

    private short getVal() {
        short tymValue;
        do {
            try {
                tymValue = scanner.nextShort();
                break;
            } catch (InputMismatchException me) {
                System.out.println("Wprowadzono nieprawidłowe znaki, spróbuj ponownie");
                scanner.next();
            }
        } while (true);
        return tymValue;
    }

    private short getMenuChoice() {
        short tymValue;
        do {
            try {
                tymValue = scanner.nextShort();
                if (tymValue == 1 || tymValue == 0)
                    break;
                else
                    System.out.println("Nie ma takiej opcji!");
            } catch (InputMismatchException me) {
                System.out.println("Akceptowane są tylko liczby! Spróbuj ponownie");
                scanner.next();
            }
        } while (true);
        return tymValue;
    }

}
